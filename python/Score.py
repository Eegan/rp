class Score:

    def __init__(self, slide1, slide2, score):
        self.score = score
        self.slide1 = slide1
        self.slide2 = slide2

    def __str__(self):
        return str(self.slide1)+" "+str(self.slide2)+" "+str(self.score)