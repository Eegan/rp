from Photo import *
"""
Functions to read or write data to files
"""
def readFile(name, p):
    with open(name) as f:
        data = f.read().splitlines()

    size = int(int(data[0])*p/100)
    photos = []
    for i in range(1,size+1):
        values = data[i].split(" ")
        tags = []
        for j in range(2,len(values)):
            tags.append(values[j])
        photos.append(Photo(i-1, values[0] == "H", tags))
    return photos

def writeFile(name, slideshow):
    f = open(name, "w")
    f.write(str(len(slideshow.slides))+"\n")
    for s in slideshow.slides:
        if s.horizontal != None:
            f.write(str(s.horizontal.id)+"\n")
        else:
            f.write(str(s.vertical[0].id)+" "+str(s.vertical[1].id)+"\n")
