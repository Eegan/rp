# -*- coding: utf-8 -*-

from gurobipy import *
from Sorting import *
from Slideshow import *
import numpy as np


def computePLNE(photos):
    C=[]
    slideshow=sortHV(photos)
    n=len(slideshow.slides)
    for i in range(n):
        c=[]
        for j in range(n):
            c.append(slideshow.slides[i].computeScoreSlide(slideshow.slides[j]))
        C.append(c)
    m = Model("Google Hashcode")
    # Contraintes de base
    X = []
    for i in range(n):
        x = []
        for j in range(n):
            x.append(m.addVar(vtype=GRB.BINARY, lb=0))
        X.append(x)

    for i in range(n):
        m.addConstr(quicksum(X[i][j] for j in range(n)) <= 1)
        m.addConstr(quicksum(X[j][i] for j in range(n)) <= 1)

    for i in range(n):
        for j in range(n):
            m.addConstr(X[i][j] + X[j][i] <= 1)

    m.addConstr(quicksum(quicksum(X[i][j] for j in range(n)) for i in range(n)) == n - 1)

    # Elimination des sous-tours
    U = []
    for i in range(n):
        U.append(m.addVar(vtype=GRB.CONTINUOUS))
    for i in range(0, n):
        m.addConstr(1 <= U[i])
        m.addConstr(U[i] <= n)

    for i in range(0, n):
        for j in [x for x in range(0, n) if x != i]:
            m.addConstr(U[i] - U[j] + 1 <= n * (1 - X[i][j]))

    m.setObjective(quicksum(quicksum(X[i][j] * C[i][j] for j in range(n)) for i in range(n)), GRB.MAXIMIZE)

    m.optimize()

    slides = []
    for i in range(n):
        slides.append(0)
    for i in range(n):
        slides[round(U[i].X)-1] = slideshow.slides[i]
    return Slideshow(slides)

def computePLNEHV(photos):
    photosH=[]
    photosV=[]
    for p in photos:
        if p.isHorizontal:
            photosH.append(p)
        else:
            photosV.append(p)
    if len(photosV) %2 !=0:
        photosV.pop()
    nbH = len(photosH)
    nbV = len(photosV)
    slides=[]
    #Ajout de toutes les slides possibles avec les photos
    for i in range(nbV):
        for j in range(i, nbV):
            if i!=j:
                slides.append(Slide(photosV[i],photosV[j]))
    #Ajoute une liste de slide incompatible si on ajoute la ième slide
    constV=[]
    for i in range(len(slides)):
        c=[]
        for j in range(len(slides)):
            if i!=j:
                if slides[i].vertical[0] in slides[j].vertical or slides[i].vertical[1] in slides[j].vertical:
                    c.append(j)
        constV.append(c)
    for p in photosH:
        slides.append(Slide(p,None))
    n=len(slides)
    #Matrice de cout
    C=[]
    for i in range(n):
        c=[]
        for j in range(n):
            c.append(slides[i].computeScoreSlide(slides[j]))
        C.append(c)
        m = Model("Google Hashcode")
    # Contraintes de base
    X = []
    for i in range(n):
        x = []
        for j in range(n):
            x.append(m.addVar(vtype=GRB.BINARY, lb=0))
        X.append(x)

    for i in range(n):
        m.addConstr(quicksum(X[i][j] for j in range(n)) <= 1)
        m.addConstr(quicksum(X[j][i] for j in range(n)) <= 1)

    for i in range(n):
        for j in range(n):
            m.addConstr(X[i][j] + X[j][i] <= 1)

    m.addConstr(quicksum(quicksum(X[i][j] for j in range(n)) for i in range(n)) == n - 1)

    # Elimination des sous-tours
    U = []
    for i in range(n):
        U.append(m.addVar(vtype=GRB.CONTINUOUS))
    for i in range(0, n):
        m.addConstr(1 <= U[i])
        m.addConstr(U[i] <= n)

    for i in range(0, n):
        for j in [x for x in range(0, n) if x != i]:
            m.addConstr(U[i] - U[j] + 1 <= n * (1 - X[i][j]))
    
    #Impose les slides horizontales dans les premières slides
    for i in range(sum([x for x in range(nbV)]), nbH+sum([x for x in range(nbV)])):
        m.addConstr(U[i]<=nbH+int(nbV/2))
    #Impose que si une slide verticale est dans les premières slides
    #les slides incompatibles doivent être dans les dernières
    print(nbV, nbH)
    print((round(nbH/(nbH+0.1))))
    #########################
    for i in range(len(constV)):

        for j in constV[i]:
            m.addConstr(U[i]+U[j]>=(2*nbH+int(nbV/2))*(round(nbH/(nbH+0.1))) + (2*nbH+2*int(nbV/2))*(1-round(nbH/(nbH+0.1))))
    #########################
    m.setObjective(quicksum(quicksum((X[i][j] * C[i][j]) for j in range(n)) for i in range(n)), GRB.MAXIMIZE)

    m.optimize()
    
    for i in range(len(U)):
        print([x.X for x in X[i]])
    for i in range(len(U)):
        print("U"+str(i),U[i].X)
    slidesres=[]
    #Ajoute les premières slides dans le slideshow
    for i in range(nbH+int(nbV/2)):
        slidesres.append(0)
    for i in range(n):
        if(round(U[i].X) <= nbH+int(nbV/2)):
            slidesres[round(U[i].X)-1] = slides[i]

    return Slideshow(slidesres)