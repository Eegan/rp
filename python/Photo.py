class Photo:
    def __init__(self, id, isHorizontal, tags):
        self.id = id
        self.isHorizontal = isHorizontal
        self.tags = tags

    def __str__(self):
        s = str(self.id)+" "+str(self.isHorizontal)
        return s
