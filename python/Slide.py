class Slide:

    def __init__(self, p1, p2):
        if(p2 == None):
            self.horizontal = p1
            self.vertical = None
            self.slideTags = p1.tags
        else:
            self.vertical = [p1,p2]
            self.horizontal = None
            self.slideTags = p1.tags
            for t in p2.tags:
                if not t in self.slideTags:
                    self.slideTags.append(t)

    def computeScoreSlide(self, slide):
        """
        compute score between this slide and the slide given as parameter
        :param slide: slide used to compute the score
        :return: score
        """
        sizeCommon = 0
        sizeL1 = 0
        sizeL2 = len(slide.slideTags)

        for t in self.slideTags:
            if t in slide.slideTags:
                sizeL2 -=1
                sizeCommon +=1
            else:
                sizeL1 +=1

        return min(sizeCommon, min(sizeL1, sizeL2))

    def computeScorePhoto(self, photo):
        """
        compute score between this slide and the photo given as parameter
        :param slide: photo used to compute the score
        :return: score
        """
        sizeCommon = 0
        sizeL1 = 0
        sizeL2 = len(photo.tags)

        for t in self.slideTags:
            if t in photo.tags:
                sizeL2 -=1
                sizeCommon +=1
            else:
                sizeL1 +=1

        return min(sizeCommon, min(sizeL1, sizeL2))

    def __str__(self):
        s=""
        if(self.vertical == None):
            s=s+str(self.horizontal)+" "

        else:
            s=s+str(self.vertical[0])+" "+str(self.vertical[1])+" "

        return s
