from Slide import *
from Slideshow import *
from Score import *
import random
import copy



def sortHV(photos):
    """
    create a slideshow with all the horizontal slides first, then vertical ones
    :param photos: list of photo used to create the slideshow
    :return: a slideshow
    """
    slidesH = []
    slidesV = []

    tempV = None

    for p in photos:
        if p.isHorizontal:
            slidesH.append(Slide(p, None))
        else:
            if tempV == None:
                tempV = p
            else:
                slidesV.append(Slide(tempV, p))
                tempV = None

    slides = slidesH+slidesV
    return Slideshow(slides)

def sortgreedy(photos):
    """
    create a slideshow, the first slide is chosen randomly, then add the slide that maximises the score
    :param photos: list of photo used to create the slideshow
    :return: a slideshow
    """
    slides = []
    photo1 = photos[random.randint(0, len(photos)-1)]
    #Add a randomly chosen slide
    if(photo1.isHorizontal):
        slides.append(Slide(photo1,None))
        photos.remove(photo1)
    #Need a 2nd vertical photo if the first is vertical
    else:
        photo2 = photos[random.randint(0, len(photos) - 1)]
        while(photo2.isHorizontal or photo2 == photo1):
            photo2 = photos[random.randint(0, len(photos) - 1)]
        slides.append(Slide(photo1, photo2))
        photos.remove(photo1)
        photos.remove(photo2)

    while len(photos)>0:
        bestScore=0
        bestPhoto=None
        #Select the photo which maximises the score
        for p in photos:
            if slides[len(slides)-1].computeScorePhoto(p) > bestScore or bestScore == 0:
                bestPhoto=p
                bestScore=slides[len(slides)-1].computeScorePhoto(p)
        #Add a slide from this photo if it's horizontal
        if bestPhoto.isHorizontal:
            slides.append(Slide(bestPhoto, None))
            photos.remove(bestPhoto)
        #Select the best 2nd vertical photo if the first one is vertical
        else:
            bestScore=0
            bestPhoto2= None
            for p in photos:
                if(not p.isHorizontal and p != bestPhoto):
                    if slides[len(slides)-1].computeScoreSlide(Slide(bestPhoto, p))>bestScore or bestScore == 0:
                        bestPhoto2 = p
                        slides[len(slides) - 1].computeScoreSlide(Slide(bestPhoto, p))
            if not bestPhoto2 == None:
                slides.append(Slide(bestPhoto, bestPhoto2))
                photos.remove(bestPhoto)
                photos.remove(bestPhoto2)
            else:
                photos.remove(bestPhoto)
    return Slideshow(slides)

def sortrand(photos):
    """
    create a randomly sorted slideshow
    :param photos: list of photo used to create the slideshow
    :return: a slideshow
    """
    tempSlideshow = sortHV(photos)
    slides = []
    while len(tempSlideshow.slides)>0:
        x=random.randint(0,len(tempSlideshow.slides)-1)
        slides.append(tempSlideshow.slides[x])
        tempSlideshow.slides.pop(x)
    return Slideshow(slides)

def searchbestneighbor(slideshow):
    bestScore = slideshow.score
    x = random.randint(0,len(slideshow.slides)-1)
    #If the slide is horizontal, try to swap with an other slide
    if slideshow.slides[x].horizontal != None:
        y = random.randint(0, len(slideshow.slides) - 1)
        while y == x:
            y = random.randint(0, len(slideshow.slides) - 1)
        slideshow.swapSlide(x, y)
        if bestScore>slideshow.score:
            slideshow.swapSlide(x, y)
    #If the slide is vertical, chose an other vertical slide and switch one of their photos
    else:
        y = random.randint(0, len(slideshow.slides) - 1)
        while y == x or slideshow.slides[y].vertical == None:
            y = random.randint(0, len(slideshow.slides) - 1)
        posX = random.randint(0,1)
        posY = random.randint(0,1)
        slideshow.swapSlideVertical(x, y, posX, posY)
        if bestScore>slideshow.score:
            slideshow.swapSlideVertical(x, y, posX, posY)

def sortmetaheuristics(photos):
    """
    create a random slideshow, switch two randomly chosen slides (or photos from vertical slides) if it increases the score.
    does it 100 000 times.
    :param photos: list of photo used to create the slideshow
    :return: a slideshow
    """
    slideshow = sortrand(photos)
    for i in range(100000):
        searchbestneighbor(slideshow)
    print(slideshow.score)
    return slideshow

def sortmetaheuristicsimproved(photos):
    """
    Step 1: create a random slideshow,
    Step 2: look for the best neighbor.
    Step 3: create a new one based on the bests duos of slides in the best neighbour found.
    Step 4: use this new slideshow and go back to Step 2
    repeat 10 times
    :param photos: list of photo used to create slideshows
    :return: the best slideshow
    """
    #First random slideshow
    bestSlideshow = sortrand(photos)
    for j in range(10):
        #Looking for the best neighbor
        for i in range(100000):
            searchbestneighbor(bestSlideshow)
            
        ratio = bestSlideshow.score/len(bestSlideshow.slides)
        added = []
        for i in range(len(bestSlideshow.slides)):
            added.append(1)
        newSlides = []
        for i in range(len(bestSlideshow.slides)-1):
            #If we have a good score, add the slide and the next slide (if they don't already exist in the list)
            if bestSlideshow.slides[i].computeScoreSlide(bestSlideshow.slides[i+1])>=ratio:
                if bestSlideshow.slides[i] in newSlides:
                    newSlides.append(bestSlideshow.slides[i+1])
                    added[i+1]=0
                else:
                    newSlides.append(bestSlideshow.slides[i])
                    newSlides.append(bestSlideshow.slides[i+1])
                    added[i]=0
                    added[i+1]=0
        #Add possibly missing slides
        for i in range(len(added)):
            if added[i] == 1:
                newSlides.append(bestSlideshow.slides[i])
        newSlideshow = Slideshow(newSlides)
        if bestSlideshow.score < newSlideshow.score:
            bestSlideshow = newSlideshow
        print(bestSlideshow.score, len(bestSlideshow.slides))
    return bestSlideshow

def kruskal(slideshow):
    scores = []
    for i in range(len(slideshow.slides)):
        for j in range(i+1,len(slideshow.slides)):
            scores.append(Score(slideshow.slides[i], slideshow.slides[j], slideshow.slides[i].computeScoreSlide(slideshow.slides[j])))
    scores.sort(key=lambda x: x.score, reverse=True)

    slides = []

    for s in scores:
        #If neither of the slides are in the list, add both at the end
        if not s.slide1 in slides and not s.slide2 in slides:
            slides.append(s.slide1)
            slides.append(s.slide2)
        #If one of them exist, add the other before or after if it increases the score.
        #Otherwise, it means the slides were added together before.
        if s.slide1 in slides and not s.slide2 in slides:
            pos = slides.index(s.slide1)
            if pos == 0:
                slides.insert(0,s.slide2)
            elif pos == len(slides)-1:
                slides.append(s.slide2)
            else:
                if slides[pos].computeScoreSlide(slides[pos+1])<s.score:
                    slides.insert(pos+1,s.slide2)
                elif slides[pos].computeScoreSlide(slides[pos-1])<s.score:
                    slides.insert(pos,s.slide2)
        if not s.slide1 in slides and s.slide2 in slides:
            pos = slides.index(s.slide2)
            if pos == 0:
                slides.insert(0,s.slide1)
            elif pos == len(slides)-1:
                slides.append(s.slide1)
            else:
                if slides[pos].computeScoreSlide(slides[pos+1])<s.score:
                    slides.insert(pos+1,s.slide1)
                elif slides[pos].computeScoreSlide(slides[pos-1])<s.score:
                    slides.insert(pos,s.slide1)
    return slides

def sortapproched(photos):
    """
    creates a slideshow based on an algorithim similar to Prim.
    :param photos: list of photo used to create the slideshow
    :return: a slideshow
    """
    slideshow = sortHV(photos)
    slides = kruskal(slideshow)

    return Slideshow(slides)

def splitlist(l,n):
    """
    """
    return [l[i::n] for i in range(n)]

def sortmixed(photos):
    """
    """

    bestSlideshow = None
    photos2 = copy.deepcopy(photos)
    randomSortPhotos = []
    #Randomly sort photos
    while len(photos2)>0:
        x = random.randint(0,len(photos2)-1)
        randomSortPhotos.append(photos2[x])
        photos2.pop(x)
        tempV = None
    randomSlides = []
    #Create slides from photos
    for p in randomSortPhotos:
        if p.isHorizontal:
            randomSlides.append(Slide(p, None))
        else:
            if tempV == None:
                tempV = p
            else:
                randomSlides.append(Slide(tempV, p))
                tempV = None

    #split slides into lists
    splited = splitlist(randomSlides, 300)
    splitedres=[]
    #Use kruskal an every list of slides
    for slides in splited:
        splitedres.append(kruskal(Slideshow(slides)))
    #Randomly chose the first list to add
    x=random.randint(0,len(splitedres)-1)
    res = splitedres[x]
    splitedres.pop(x)
    #Add other lists so it will maximize our score
    while len(splitedres)>0:
        bestscore=0
        bestsplit=None
        for split in splitedres:
            if bestsplit == None or res[len(res)-1].computeScoreSlide(split[0])>bestscore:
                bestsplit = split
        res = res+bestsplit
        splitedres.remove(bestsplit)
    slideshow = Slideshow(res)
    print(slideshow.score, len(slideshow.slides))
    if bestSlideshow == None or slideshow.score>bestSlideshow.score:
        bestSlideshow = slideshow
    return bestSlideshow
    