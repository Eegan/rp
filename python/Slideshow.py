class Slideshow:

    def __init__(self, slides):
        self.slides = slides
        self.score = self.computeScore()

    def computeScore(self):
        score = 0
        for i in range(len(self.slides)-1):
            score+=self.slides[i].computeScoreSlide(self.slides[i+1])
        return score

    def swapSlide(self, x, y):

        self.substractScore(x,y)

        temp=self.slides[x]
        self.slides[x]=self.slides[y]
        self.slides[y]=temp

        self.addScore(x, y)

    def swapSlideVertical(self, x, y, posX, posY):

        self.substractScore(x, y)

        p = self.slides[x].vertical[posX]
        self.slides[x].vertical[posX] = self.slides[y].vertical[posY]
        self.slides[y].vertical[posY] = p

        self.addScore(x, y)

    def substractScore(self, x, y):
        if x>0:
            self.score -= self.slides[x].computeScoreSlide(self.slides[x-1])
        if x<len(self.slides)-1:
            self.score -= self.slides[x].computeScoreSlide(self.slides[x+1])
        if y>0:
            self.score -= self.slides[y].computeScoreSlide(self.slides[y-1])
        if y<len(self.slides)-1:
            self.score -= self.slides[y].computeScoreSlide(self.slides[y+1])


    def addScore(self, x, y):
        if x>0:
            self.score += self.slides[x].computeScoreSlide(self.slides[x-1])
        if x<len(self.slides)-1:
            self.score += self.slides[x].computeScoreSlide(self.slides[x+1])
        if y>0:
            self.score += self.slides[y].computeScoreSlide(self.slides[y-1])
        if y<len(self.slides)-1:
            self.score += self.slides[y].computeScoreSlide(self.slides[y+1])


    def __str__(self):
        s = ""
        for i in range(len(self.slides)):
            s=s+str(self.slides[i])+"\n"
        return s+str(self.score)