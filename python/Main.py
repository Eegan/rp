from FileParser import *
from Sorting import *
from plne import *
import time
import numpy as np

photos = readFile("../data/d_pet_pictures.txt", 0.01)
start = time.time()
slideshow = computePLNEHV(photos)
end = time.time()
print(end-start)
for i in range(len(slideshow.slides)):
    print(slideshow.slides[i])
print("Score:", slideshow.score)

